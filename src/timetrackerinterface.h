#pragma once

// Abstract base of time tracker interface.
// interface is confusing, so use pythonic ABC
struct TimeTrackerInterfaceABC{
    virtual ~TimeTrackerInterfaceABC(){}
    virtual bool activateTask(QString const & name) = 0;
    virtual bool isTaskActive(QString const & name) = 0;
    virtual bool deactivateTask(QString const & name) =0;

    // if the tracker application is running
    virtual bool isTrackerRunning() const =0;

    virtual bool createTask(QString name)=0;

    virtual void update() { }

};
