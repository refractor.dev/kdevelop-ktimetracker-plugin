#ifndef KDEVTIMETRACKER_H
#define KDEVTIMETRACKER_H

#include <interfaces/iplugin.h>
#include <QTimer>

#include "ktimetrackerinterface.h"

class kdevTimetracker : public KDevelop::IPlugin
{
    Q_OBJECT
    // to make the tasks a bit more unique
    static constexpr const char* prefix = "kdevsession_";

public:
    // KPluginFactory-based plugin wants constructor with this signature
    kdevTimetracker(QObject* parent, const QVariantList& args);
    ~kdevTimetracker();

    void update();
    // call when activity is detected
    void tick();

private:
    QString taskName() const;
    QTimer m_inactivityTimer;
    QString m_sessionName;
    std::unique_ptr<TimeTrackerInterfaceABC> m_tracker { new KTimeTrackerInterface };

protected Q_SLOTS:
    void handldeSessionNameChange();

    void becomeActive();

    // user is not actively working on this session
    // so stop timers
    void becomeInactive();

};

#endif // KDEVTIMETRACKER_H
