#include "kdevtimetracker.h"

#include <chrono>

using namespace std::literals::chrono_literals;

#include <debug.h>

#include <KPluginFactory>
#include <kdevplatform/interfaces/icore.h>
#include <kdevplatform/interfaces/isession.h>
#include <kdevplatform/interfaces/idocumentcontroller.h>
#include <kdevplatform/interfaces/iuicontroller.h>

// millisecond for qtimer
static constexpr std::chrono::milliseconds inactivityInterval = 5min;

K_PLUGIN_FACTORY_WITH_JSON(kdevTimetrackerFactory, "kdevtimetracker.json", registerPlugin<kdevTimetracker>(); )

kdevTimetracker::kdevTimetracker(QObject *parent, const QVariantList& args)
    : KDevelop::IPlugin(QStringLiteral("kdevtimetracker"), parent)
    , m_sessionName{core()->activeSession()->name()}
{
    Q_UNUSED(args);
    m_inactivityTimer.setInterval(inactivityInterval);
    m_inactivityTimer.setSingleShot(true);
    tick();

    connect(&m_inactivityTimer, &QTimer::timeout,
            this, &kdevTimetracker::becomeInactive);

    connect(core()->activeSession(), &KDevelop::ISession::sessionUpdated,
            this, &kdevTimetracker::handldeSessionNameChange);

    connect(core()->documentController(), &KDevelop::IDocumentController::documentContentChanged,
            this, &kdevTimetracker::tick);

    becomeActive();
}
kdevTimetracker::~kdevTimetracker() noexcept
{
    // stop tracking on exit
    becomeInactive();
}


QString kdevTimetracker::taskName() const
{
    return prefix + core()->activeSession()->name();
}

void kdevTimetracker::tick()
{
    if(!m_inactivityTimer.isActive())
    {
        becomeActive();
    }
    qCDebug(PLUGIN_KDEVTIMETRACKER) << "tick";
    // actually it is a restart
    m_inactivityTimer.start();
}


void kdevTimetracker::update()
{
}


void kdevTimetracker::becomeActive()
{
    m_tracker->activateTask(taskName());
}

void kdevTimetracker::becomeInactive()
{
    core()->uiController()->showErrorMessage("time tracking stopped: " + taskName(), 5);
    m_tracker->deactivateTask(taskName());
}

void kdevTimetracker::handldeSessionNameChange()
{
    if(m_sessionName != core()->activeSession()->name())
    {
        becomeInactive();
        // change the name
        m_sessionName = core()->activeSession()->name();
        // start tracking with new name
        becomeActive();
    }
}




// needed for QObject class created from K_PLUGIN_FACTORY_WITH_JSON
#include "kdevtimetracker.moc"
