#pragma once
#include <string_view>

#include <QDebug>
#include <QDBusInterface>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusReply>

#include "timetrackerinterface.h"
#include "debug.h"

struct KTimeTrackerInterface: public TimeTrackerInterfaceABC{
    KTimeTrackerInterface(){
    }

    bool deactivateTask(QString const & name) final
    {
        qCDebug(PLUGIN_KDEVTIMETRACKER) << "deactivating task" <<  name;
        QDBusReply<bool> res = m_iface.call("stopTimerForTaskName", name);
        return res ;
    }
    bool isTaskActive(QString const & name) final
    {
        auto res = m_iface.call("isTaskNameActive", name);
        QDBusReply<bool> bres = res;
        if(!bres.isValid())
        {
            qCWarning(PLUGIN_KDEVTIMETRACKER) << " not connected" << res;
        }
        return bres.value();
    }

    bool activateTask(QString const & name) final
    {
        if(isTaskActive(name))
        {
            qCDebug(PLUGIN_KDEVTIMETRACKER) << "task" << name << "already running";
            return true;
        }
        if(!isTrackerRunning())
        {
            qCWarning(PLUGIN_KDEVTIMETRACKER) << "dbus not connected" << m_iface.lastError();
        }
        qCDebug(PLUGIN_KDEVTIMETRACKER) << "activating task" <<  name;
        auto res = m_iface.call("startTimerForTaskName", name);
        QDBusReply<bool> bres = res; 
        if( !bres.isValid() )
        {
            qCWarning(PLUGIN_KDEVTIMETRACKER) << "start timer failed" << res.errorMessage();
            return false;
        }
        if(!bres.value())
        {
            createTask(name);
            res = m_iface.call("startTimerForTaskName", name);
        }
        if(!bres.value())
            qCWarning(PLUGIN_KDEVTIMETRACKER) << "start timer failed" << m_iface.lastError().message();
        return bres.value();
    }

    // if the tracker application is running
    inline bool isTrackerRunning() const final {
        return m_iface.isValid();
    }

    bool createTask(QString name) final {
        auto res = m_iface.call(QDBus::CallMode::Block, "addTask", name);
        QDBusReply<bool> bres = res; 
        if(!bres.isValid())
        {
            qCWarning(PLUGIN_KDEVTIMETRACKER) << "createTask failed" << res.errorMessage();
            return false;
        }
        return bres.value();
    }

private:
    QDBusInterface m_iface{"org.kde.ktimetracker", "/KTimeTracker", "org.kde.ktimetracker.ktimetracker"};
    bool m_isTrackerRunning{false};
};
